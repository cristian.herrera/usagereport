package DataAccess;
import java.io.BufferedReader; 
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets; 
import java.nio.file.Files; 
import java.nio.file.Path; 
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Model.Employee;
import Model.Order;
import Model.Product;
import Model.ProductUsage;
import Model.UsageReport; 

public class CSVReader { 
    static final int ID_POSITION = 0;
    static final int DATE_POSITION = 1;
    static final int TOTAL_MINUTES_POSITION = 2;
    static final int TOTAL_DATA_POSITION = 3;

    public static void readEmployeesInfoFromCSV(String sFileName, Map<Integer, Employee> mEmployees) {
        Path pathToFile = Paths.get(sFileName); 

        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            String sLine = br.readLine(); // read the first line from the text file 
            sLine = br.readLine(); // tittle line
            // loop until all lines are read 
            while (sLine != null) { 
                String[] saAttributes = sLine.split(",");
                mEmployees.put(Integer.parseInt(saAttributes[ID_POSITION]), createEmployee(saAttributes));

                sLine = br.readLine();
            } 
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } 
    }

    public static void readProductUsageFromCSV(String sFileName, Map<Integer, Employee> mEmployees, UsageReport report) {
        Path pathToFile = Paths.get(sFileName);
        String[] saAttributes;

        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            String sLine = br.readLine(); // read the first line from the text file 
            sLine = br.readLine(); // tittle line
            // loop until all lines are read 
            while (sLine != null) { 
                saAttributes = sLine.split(",");
                Employee employee = mEmployees.get(Integer.parseInt(saAttributes[ID_POSITION]));
                Map<Date, ProductUsage> mProductUsage = employee.getProductUsage();

                createProductUsage(saAttributes, mProductUsage);
                employee.setProductUsage(mProductUsage);
                mEmployees.replace(Integer.parseInt(saAttributes[ID_POSITION]), employee);

                report.setTotalMinutes(report.getTotalMinutes().add(BigInteger.valueOf(Integer.parseInt(saAttributes[TOTAL_MINUTES_POSITION]))));
                report.setTotalData(report.getTotalData().add(BigDecimal.valueOf(Float.parseFloat(saAttributes[TOTAL_DATA_POSITION]))));

                sLine = br.readLine(); 
            } 
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } 
    }

    private static void createProductUsage(String[] saAttributes, Map<Date, ProductUsage> mProductUsage) {
        ProductUsage usage = new ProductUsage();
        Date date;
        try {
            date = new SimpleDateFormat("MM/dd/yyyy").parse(saAttributes[DATE_POSITION]);
            usage.setDate(date);
            usage.setTotalMinutes(Integer.parseInt(saAttributes[TOTAL_MINUTES_POSITION]));
            usage.setTotalData(Float.parseFloat(saAttributes[TOTAL_DATA_POSITION]));

            mProductUsage.put(date, usage);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static Employee createEmployee(String[] saAttributes) {
        Employee employee = new Employee();
        Map<Date, ProductUsage> mProductUsage = new HashMap<>();

        employee.setId(Integer.parseInt(saAttributes[ID_POSITION]));
        employee.setName(saAttributes[DATE_POSITION]);
        employee.setOrder(createOrder(saAttributes));
        employee.setProductUsage(mProductUsage);

        return employee;
    }

    private static Order createOrder(String[] saAttributes) {
        Order order = new Order();
        Date date;
        try {
            date = new SimpleDateFormat("yyyyMMdd").parse(saAttributes[TOTAL_MINUTES_POSITION]);
            order.setPurchaseDate(date);
            order.setProduct(createProduct(saAttributes[TOTAL_DATA_POSITION]));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return order;
    }

    private static Product createProduct(String data) {
        Product product = new Product();
        product.setBrandName(data.substring(0, data.indexOf(" ")));
        product.setModel(data.substring(data.indexOf(" ")+1));

        return product;
    }

}
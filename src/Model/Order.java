package Model;

import java.util.Date;

public class Order {
    private Date purchaseDate;
    private Product product;

    public void setPurchaseDate(Date date) { this.purchaseDate = date; }
    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setProduct(Product product) { this.product = product; }
    public Product getProduct() {
        return product;
    }
}
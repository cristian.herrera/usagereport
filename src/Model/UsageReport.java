package Model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

public class UsageReport {
    private Date date;
    private int year;
    private BigInteger numberOfPhones;
    private BigInteger totalMinutes;
    private BigDecimal totalData;
    private BigInteger averageMinutes;
    private BigDecimal averageData;
    private Map<Integer, Employee> employees;

    public void setDate(Date date) { this.date = date; }
    public Date getDate() {
        return date;
    }

    public void setYear(int year) { this.year = year; }
    public int getYear() {
        return year;
    }

    public void setNumberOfPhones(BigInteger numberOfPhones) { this.numberOfPhones = numberOfPhones; }
    public BigInteger getNumberOfPhones() {
        return numberOfPhones;
    }

    public void setTotalMinutes(BigInteger totalMinutes) { this.totalMinutes = totalMinutes; }
    public BigInteger getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalData(BigDecimal totalData) { this.totalData = totalData; }
    public BigDecimal getTotalData() {
        return totalData;
    }

    public void setAverageMinutes(BigInteger averageMinutes) { this.averageMinutes = averageMinutes; }
    public BigInteger getAverageMinutes() {
        return averageMinutes;
    }

    public void setAverageData(BigDecimal averageData) { this.averageData = averageData; }
    public BigDecimal getAverageData() {
        return averageData;
    }

    public void setEmployees(Map<Integer, Employee> employees) { this.employees = employees; }
    public Map<Integer, Employee> getEmployees() {
        return employees;
    }
}

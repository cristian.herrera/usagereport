package Model;
public class Product {
    private String brandName;
    private String model;

    public void setBrandName(String name) { this.brandName = name; }
    public String getBrandName() {
        return brandName;
    }

    public void setModel(String model) { this.model = model; }
    public String getModel() {
        return model;
    }
}

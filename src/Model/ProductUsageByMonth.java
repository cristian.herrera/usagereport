package Model;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ProductUsageByMonth {
    private int month;
    private BigInteger totalMinutes;
    private BigDecimal totalData;

    public void setMonth(int month) { this.month = month; }
    public int getMonth() {
        return month;
    }

    public void setTotalMinutes(BigInteger totalMinutes) { this.totalMinutes = totalMinutes; }
    public BigInteger getTotalMinutes() { return totalMinutes; }

    public void setTotalData(BigDecimal totalData) { this.totalData = totalData; }
    public BigDecimal getTotalData() { return totalData; }
}
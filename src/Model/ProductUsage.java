package Model;

import java.util.Date;

public class ProductUsage {
    private Date date;
    private int totalMinutes;
    private float totalData;

    public void setDate(Date date) { this.date = date; }
    public Date getDate() {
        return date;
    }

    public void setTotalMinutes(int totalMinutes) { this.totalMinutes = totalMinutes; }
    public int getTotalMinutes() { return totalMinutes; }

    public void setTotalData(float totalData) { this.totalData = totalData; }
    public float getTotalData() { return totalData; }
}
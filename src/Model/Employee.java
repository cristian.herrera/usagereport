package Model;
import java.util.Date;
import java.util.Map;

public class Employee {
    private int id;
    private String name;
    private Order order;
    private Map<Date, ProductUsage> productUsage;
    private Map<Integer, ProductUsageByMonth> productUsageByMonth;

    public void setId(int id) { this.id = id; }
    public int getId() {
        return id;
    }

    public void setName(String name) { this.name = name; }
    public String getName() {
        return name;
    }

    public void setOrder(Order order) { this.order = order; }
    public Order getOrder() {
        return order;
    }

    public void setProductUsage(Map<Date, ProductUsage> productUsage) { this.productUsage = productUsage; }
    public Map<Date, ProductUsage> getProductUsage() {
        return productUsage;
    }

    public void setProductUsageByMonth(Map<Integer, ProductUsageByMonth> productUsageByMonth) { this.productUsageByMonth = productUsageByMonth; }
    public Map<Integer, ProductUsageByMonth> getProductUsageByMonth() {
        return productUsageByMonth;
    }
}

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;

import DataAccess.CSVReader;

import java.util.Date;

import Model.Employee;
import Model.ProductUsage;
import Model.ProductUsageByMonth;
import Model.UsageReport;
import Utilities.PrintJobWatcher;

public class App {
    public static void main(String[] args) throws Exception {
        Map<Integer, Employee> mEmployees = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int iYear = 0;
        StringBuilder sbToPrint = new StringBuilder();
        
        System.out.print("In order to generate a usage report, please specify the year: ");
        iYear = Integer.parseInt(reader.readLine());

        UsageReport report = new UsageReport();
        initializeReport(report, iYear);

        CSVReader.readEmployeesInfoFromCSV("files/CellPhone.csv", mEmployees);
        CSVReader.readProductUsageFromCSV("files/CellPhoneUsageByMonth.csv", mEmployees, report);

        processEmployeesData(mEmployees, iYear);

        printReportHeaderInfo(mEmployees, report, sbToPrint);
        printReportDetailsInfo(mEmployees, sbToPrint);

        sendToPrinter(sbToPrint.toString());
    }

    private static void printReportHeaderInfo(Map<Integer, Employee> mEmployees, UsageReport report, StringBuilder sbToPrint) {
        sbToPrint.append("\n");
        sbToPrint.append("---- Header section -----\n");
        sbToPrint.append("Report run date: "+ new Date()+"\n");
        sbToPrint.append("Number of phones: "+ mEmployees.size()+"\n");
        sbToPrint.append("Total minutes: "+ report.getTotalMinutes().toString()+"\n");
        report.setTotalData(report.getTotalData().setScale(2, BigDecimal.ROUND_HALF_EVEN));
        sbToPrint.append("Total data: "+ report.getTotalData()+"\n");
        sbToPrint.append("Average minutes: "+ report.getTotalMinutes().divide(BigInteger.valueOf(mEmployees.size()))+"\n");
        sbToPrint.append("Average data: "+ report.getTotalData().divide(BigDecimal.valueOf(mEmployees.size()), 2, RoundingMode.HALF_UP)+"\n");
        sbToPrint.append("\n");
        System.out.println(sbToPrint.toString());
    }

    private static void printReportDetailsInfo(Map<Integer, Employee> mEmployees, StringBuilder sbToPrint) {
        String sSpace = "   ";
        sbToPrint.setLength(0);
        sbToPrint.append("---- Details section -----\n");
        sbToPrint.append("\n");

        for (Map.Entry<Integer, Employee> employee : mEmployees.entrySet()) {
            sSpace = "   ";

            sbToPrint.append("Employee id: "+ employee.getKey()+"\n");
            sbToPrint.append("Employee name: "+ employee.getValue().getName()+"\n");
            sbToPrint.append("Model: "+ employee.getValue().getOrder().getProduct().getModel()+"\n");
            sbToPrint.append("Purchase date: "+ employee.getValue().getOrder().getPurchaseDate()+"\n");
            sbToPrint.append("---- Minutes usage -----\n");
            sbToPrint.append("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec\n");
            for (Map.Entry<Integer, ProductUsageByMonth> productUsage : employee.getValue().getProductUsageByMonth().entrySet()) {
                if (productUsage.getKey() == 1)
                    sbToPrint.append(productUsage.getValue().getTotalMinutes());
                else {
                    sbToPrint.append(sSpace + productUsage.getValue().getTotalMinutes());
                    if (productUsage.getValue().getTotalMinutes().toString().length() == 1)
                        sSpace = "   ";
                    else if (productUsage.getValue().getTotalMinutes().toString().length() == 2)
                        sSpace = "  ";
                    else 
                        sSpace = " ";
                }
            }
            sbToPrint.append("\n");

            sSpace = "";
            sbToPrint.append("---- Data usage -----\n");
            sbToPrint.append("Jan   Feb   Mar   Apr   May   Jun   Jul   Aug   Sep   Oct   Nov   Dec\n");
            for (Map.Entry<Integer, ProductUsageByMonth> productUsage : employee.getValue().getProductUsageByMonth().entrySet()) {
                if (productUsage.getKey() == 1)
                    sbToPrint.append(productUsage.getValue().getTotalData().setScale(2, BigDecimal.ROUND_HALF_EVEN));
                else 
                    sbToPrint.append(sSpace + productUsage.getValue().getTotalData().setScale(2, BigDecimal.ROUND_HALF_EVEN));
                
                sSpace = "  ";
            }
            sbToPrint.append("\n");
            sbToPrint.append("---------------------------\n");
            sbToPrint.append("\n");
        }
        System.out.println(sbToPrint.toString());
    }

    public static void sendToPrinter(String data) throws PrintException, IOException {
        String defaultPrinter = PrintServiceLookup.lookupDefaultPrintService().getName();
        System.out.println("Default printer: " + defaultPrinter);
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();
    
        InputStream is = new ByteArrayInputStream(data.getBytes("UTF8"));
    
        PrintRequestAttributeSet  pras = new HashPrintRequestAttributeSet();
        pras.add(new Copies(1));
    
        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc doc = new SimpleDoc(is, flavor, null);
        DocPrintJob job = service.createPrintJob();
    
        PrintJobWatcher pjw = new PrintJobWatcher(job);
        job.print(doc, pras);
        pjw.waitForDone();
        is.close();
    }

    private static void processEmployeesData(Map<Integer, Employee> mEmployees, int iYear) {
        int iMonthIndex = 0;
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MM");

        for (Map.Entry<Integer, Employee> employee : mEmployees.entrySet()) {
            Map<Integer, ProductUsageByMonth> productMonthlyUsage = new HashMap<>();
            BigInteger[] iTotalMinutes = new BigInteger[12];
            BigDecimal[] dTotalData = new BigDecimal[12];
            initializeTotalMinutesArray(iTotalMinutes);
            initializeTotalDataArray(dTotalData);

            for (Map.Entry<Date, ProductUsage> productUsage : employee.getValue().getProductUsage().entrySet()) {
                if (yearFormat.format(productUsage.getKey()).equals(String.valueOf(iYear))) {
                    iMonthIndex = Integer.parseInt(monthFormat.format(productUsage.getKey()))-1;
                    iTotalMinutes[iMonthIndex] = iTotalMinutes[iMonthIndex].add(BigInteger.valueOf(productUsage.getValue().getTotalMinutes()));
                    dTotalData[iMonthIndex] = dTotalData[iMonthIndex].add(BigDecimal.valueOf(productUsage.getValue().getTotalData()));
                }
            }
            
            addProductUsageByMonth(iTotalMinutes, dTotalData, productMonthlyUsage);
            employee.getValue().setProductUsageByMonth(productMonthlyUsage);
        }
    }

    private static void initializeReport(UsageReport report, int iYear) {
        report.setDate(new Date());
        report.setYear(iYear);
        report.setNumberOfPhones(BigInteger.ZERO);
        report.setTotalMinutes(BigInteger.ZERO);
        report.setTotalData(BigDecimal.ZERO);
        report.setAverageMinutes(BigInteger.ZERO);
        report.setAverageData(BigDecimal.ZERO);
    }

    private static void addProductUsageByMonth(BigInteger[] iTotalMinutes, BigDecimal[] dTotalData, Map<Integer, ProductUsageByMonth> productMonthlyUsage) {
        for (int i=1; i < 13; i++) {
            ProductUsageByMonth usageByMonth = new ProductUsageByMonth();
            usageByMonth.setMonth(i);
            usageByMonth.setTotalMinutes(iTotalMinutes[i-1]);
            usageByMonth.setTotalData(dTotalData[i-1]);
            productMonthlyUsage.put(i, usageByMonth);
        }
    }

    private static void initializeTotalMinutesArray(BigInteger[] iTotalMinutes) {
        for (int i=0; i < iTotalMinutes.length; i++) {
            iTotalMinutes[i] = BigInteger.ZERO;
        }
    }

    private static void initializeTotalDataArray(BigDecimal[] dTotalData) {
        for (int i=0; i < dTotalData.length; i++) {
            dTotalData[i] = BigDecimal.ZERO;
        }
    }
}

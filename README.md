## Folder Structure

The workspace contains 3 folders by default, where:

- `src`: the folder to maintain sources.
- `lib`: the folder to maintain dependencies.
- `files`: the folder that contains csv files.

Meanwhile, the compiled output files will be generated in the `bin` folder by default.

## Design choices

- Use of OOP.
- Separation of responsabilities through folders / layers.
- Use of coding standards (camel case, etc).
- Use of clean code principles.

## Assumptions

- 1 employee has only 1 order.
- 1 order has only 1 product (phone).
